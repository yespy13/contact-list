<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class CarApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  contacts (
                id	TEXT NOT NULL,
                name	TEXT NOT NULL,
                surname	TEXT NOT NULL,
                phoneNumber	TEXT NOT NULL,
                email	TEXT NOT NULL
            );
            INSERT INTO contacts VALUES ('1','Juanito', 'Paribari Pepito','617755898','miau@gmail.com');
            INSERT INTO contacts VALUES ('2','Juancar', 'Sámano Castro','666777888','jsc@gmail.com');
        ");
        // $this->withoutExceptionHandling();
    }

    public function testGetContacts()
    {
        $this->json('GET', 'api/contacts')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id' => '1',
                    'name' => 'Juanito',
                    'surname' => 'Paribari Pepito',
                    'phoneNUmber' => '617755898',
                    'email' => 'miau@gmail.com'
                ],
                [
                    'id' => '2',
                    'name' => 'Juancar',
                    'surname' => 'Sámano Castro',
                    'phoneNumber' => '666777888',
                    'email' => 'jsc@gmail.com'
                ],
            ]);
    }

    public function testGetContact()
    {
        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'id' => '1',
                    'name' => 'Juanito',
                    'surname' => 'Paribari Pepito',
                    'phoneNumber' => '617755898',
                    'email' => 'miau@gmail.com'
                ]
            );
    }

    public function testPostContact()
    {
        $this->json('POST', 'api/contacts', [
            'id' => '99',
            'name' => 'Mario',
            'surname' => 'Noes Luigi',
            'phoneNumber' => '671189775',
            'email' => 'mariobros@gmail.com'
        ])->assertStatus(200);

        $this->json('GET', 'api/contacts/99')
            ->assertStatus(200)
            ->assertJson([
                'id' => '99',
                'name' => 'Mario',
                'surname' => 'Noes Luigi',
                'phoneNumber' => '671189775',
                'email' => 'mariobros@gmail.com'
            ]);
    }

    public function testPut()
    {
        $data = [
            'id' => '1',
            'name' => 'Juanillo',
            'surname' => 'Paribari Pepito',
            'phoneNumber' => '617755898',
            'email' => 'miau@gmail.com'
        ];

        $expected = [
            'id' => '1',
            'name' => 'Juanillo',
            'surname' => 'Paribari Pepito',
            'phoneNumber' => '617755898',
            'email' => 'miau@gmail.com'
        ];

        $this->json('PUT', 'api/contacts/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

    public function testPatch()
    {
        $this->json('PATCH', 'api/contacts/1', [
            'name' => 'Felipe',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'Felipe',
                'surname' => 'Paribari Pepito',
                'phoneNumber' => '617755898',
                'email' => 'miau@gmail.com'
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'registration' => 'Felipe'
            ]);

        $this->json('PATCH', 'api/contacts/1', [
            'email' => 'felipito@gmail.com'
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'Felipe',
                'surname' => 'Paribari Pepito',
                'phoneNumber' => '617755898',
                'email' => 'felipito@gmail.com'
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'Felipe',
                'surname' => 'Paribari Pepito',
                'phoneNumber' => '617755898',
                'email' => 'felipito@gmail.com'
            ]);
    }

    public function testDeleteContacts()
    {
        $this->json('GET', 'api/contacts/1')->assertStatus(200);

        $this->json('DELETE', 'api/contacts/1')->assertStatus(200);

        $this->json('GET', 'api/contacts/1')->assertStatus(404);
    }

    public function testGetContactsNotExist()
    {
        // $this->json('GET', 'api/contacts/22')->assertStatus(404);

        $this->json('DELETE', 'api/contacts/22')->assertStatus(404);

        $this->json('PUT', 'api/contacts/22', 
            [
                'id' => '1',
                'name' => 'Juanito',
                'surname' => 'Paribari Pepito',
                'phoneNUmber' => '617755898',
                'email' => 'miau@gmail.com'
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/contacts/22', [
            'name' => 'Felipito'
        ])
            ->assertStatus(404);
    }

}