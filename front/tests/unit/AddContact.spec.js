import { shallowMount } from "@vue/test-utils";
import AddContact from "@/components/AddContact.vue";

test("Add contact test.", async () => {
  const wrapper = shallowMount(AddContact);
  expect(wrapper.emitted().search).toBe(undefined);

  // Simula lo que hace el user.
  const input1 = wrapper.find(".input-name-add");
  const input2 = wrapper.find(".input-surname-add");
  const input3 = wrapper.find(".input-phone-add");
  const input4 = wrapper.find(".input-email-add");
  const save = wrapper.find(".btn-save-add");

  input1.setValue("Yeray");
  input2.setValue("Espinosa");
  input3.setValue("632547809");
  input4.setValue("yeray.esp@gmail.com");

  await wrapper.vm.$nextTick();
  save.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()["saveClickAddEvent"].length).toBe(1);
  expect(wrapper.vm.contact.name).toBe("Yeray");
  expect(wrapper.vm.contact.surname).toBe("Espinosa");
  expect(wrapper.vm.contact.phoneNumber).toBe("632547809");
  expect(wrapper.vm.contact.email).toBe("yeray.esp@gmail.com");
  });
