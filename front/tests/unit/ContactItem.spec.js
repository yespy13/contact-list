import { shallowMount } from '@vue/test-utils'
import ContactItem from '@/components/ContactItem.vue'

test("Verifica que saca el nombre debido", () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: {
                name: 'Nombre',
                surname: "Apellido"
            }
        }
    })
    expect(wrapper.text()).toContain('Nombre Apellido');
})

test("emite evento click seeContact", async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: {
                id:"1",
                name: 'Nombre',
                surname: "Apellido"
            }
        }
    })

    expect(wrapper.emitted().seeContact).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('a')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted().seeContact.length).toBe(1)
    expect(wrapper.emitted().seeContact[0]).toEqual(['1'])
});

test("emite evento click editContact", async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: {
                id:"1",
                name: 'Nombre',
                surname: "Apellido"
            }
        }
    })

    expect(wrapper.emitted().editContact).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#edit')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted().editContact.length).toBe(1)
    expect(wrapper.emitted().editContact[0]).toEqual(['1'])
});

test("emite evento click editContact", async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: {
                id:"1",
                name: 'Nombre',
                surname: "Apellido"
            }
        }
    })

    expect(wrapper.emitted().deleteContact).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#delete')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted().deleteContact.length).toBe(1)
    expect(wrapper.emitted().deleteContact[0]).toEqual(['1'])
});

test('Crear un Computed para meter en fullName la concatenación de name y surname.', async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            contact: {
                name: 'Nombre',
                surname: 'Apellido'
            },
        },    
    })
    await wrapper.vm.$nextTick()    
    expect(wrapper.vm.fullName).toContain("Nombre Apellido")
})