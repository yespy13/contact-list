import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";
import AddContact from "@/components/AddContact.vue";

test("si funciona la function generateContactListData ", async() => {
    const MockApiCall = jest.fn();
    const mockContacts = [{
            id: "1",
            name: "Jokin",
            surname: "Etxegarai Lukerne",
            phoneNumber: "617795182",
            email: "j.etxegarai@gmail.com"
        },
        {
            id: "2",
            name: "Aiala",
            surname: "Zaramaga Andrade",
            phoneNumber: "651999247",
            email: "az.andrade@gmail.com"
        }
    ];
    MockApiCall.mockReturnValue(mockContacts);
    const wrapper = shallowMount(App, {
        data() {
            return {
                contactListData: null
            };
        },
        methods: {
            ...App.methods,
            generateContactListData: MockApiCall
        }
    });

    expect(wrapper.vm.contactListData).toEqual(null);

    await wrapper.vm.$nextTick();

    expect(MockApiCall).toHaveBeenCalled();

    expect(wrapper.vm.contactListData).toEqual(mockContacts);
});