import { shallowMount } from "@vue/test-utils";
import ContactView from "@/components/ContactView.vue";

test('Pinta los props y el watch funciona', async () => {
    const wrapper = shallowMount(ContactView, {
        propsData: {
            contact: {
                id: '999',
                name: 'Felisa',
                surname: 'Yáñez Domínguez',
                phoneNumber: "617733551",
                email: "feyado@gmail.com",
            },
        },
    });
    
    await wrapper.vm.$nextTick();
    
    expect(wrapper.vm.localContact.name).toBe('Felisa');
    expect(wrapper.vm.localContact.surname).toBe('Yáñez Domínguez');
    expect(wrapper.vm.localContact.phoneNumber).toBe('617733551');
    expect(wrapper.vm.localContact.email).toBe('feyado@gmail.com');
})

test('Se cambia la prop SelectedContact cuando se pinta nuevo input', async () => {
    const wrapper = shallowMount(ContactView, {
        propsData: {
            contact: {
                id: '1',
                name: null,
                surname: null,
                phoneNumber: null,
                email: null,
            },
        },
    })    
    const nameInput = wrapper.findAll('.input-name').wrappers
    nameInput[0].setValue('Juanito')
    const surnameInput = wrapper.findAll('.input-surname').wrappers
    surnameInput[0].setValue('Cachiporro Callejón')
    const telephoneInput = wrapper.findAll('.input-phone').wrappers
    telephoneInput[0].setValue('611789777')
    const emailInput = wrapper.findAll('.input-email').wrappers
    emailInput[0].setValue('juca2@gmail.com')
    
    expect(wrapper.vm.localContact.name).toBe('Juanito')
    expect(wrapper.vm.localContact.surname).toBe('Cachiporro Callejón')
    expect(wrapper.vm.localContact.phoneNumber).toBe('611789777')
    expect(wrapper.vm.localContact.email).toBe('juca2@gmail.com')
})