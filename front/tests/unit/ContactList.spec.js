import { shallowMount } from '@vue/test-utils'
import ContactList from '@/components/ContactList.vue'
import ContactItem from "@/components/ContactItem.vue";

test("pinta todo list vacío", () => {
    const wrapper = shallowMount(ContactList, {
        propsData: {
            contacts: []
        }
    })
    const contactList = wrapper.findAll(ContactItem).wrappers

    expect(contactList.length).toBe(0)

})

test("pinta todo list con contactos", () => {
    const contacts = [
        { name: 'Nombre1', surname: "Apellido1" },
        { name: 'Nombre2', surname: "Apellido2" },
        { name: 'Nombre3', surname: "Apellido3" },
        { name: 'Nombre4', surname: "Apellido4" },
    ]
    const wrapper = shallowMount(ContactList, {
        propsData: {
            contacts: contacts
        }
    })
    const contactList = wrapper.findAll(ContactItem).wrappers
    expect(contactList.length).toBe(4)
})

test("Comprueba que envía a App lo que recibe de ContactItem (seeContact)", async () => {
    const receivedContacts = [
        { name: 'Nombre1', surname: "Apellido1" },
        { name: 'Nombre2', surname: "Apellido2" },
        { name: 'Nombre3', surname: "Apellido3" },
        { name: 'Nombre4', surname: "Apellido4" },
    ]
    const wrapper = shallowMount(ContactList, {
        propsData: {
            contacts: receivedContacts
        }
    })

    const item = wrapper.findAll(".prueba").wrappers

    expect(wrapper.emittedByOrder()).toEqual([])

    item[0].vm.$emit('seeContact', '3')
    await wrapper.vm.$nextTick()

    expect(wrapper.emittedByOrder()).toEqual([{name:"seeContact", args:['3']}])
})

test("Comprueba que envía a App lo que recibe de ContactItem (editContactList)", async () => {
    const receivedContacts = [
        { name: 'Nombre1', surname: "Apellido1" },
        { name: 'Nombre2', surname: "Apellido2" },
        { name: 'Nombre3', surname: "Apellido3" },
        { name: 'Nombre4', surname: "Apellido4" },
    ]
    const wrapper = shallowMount(ContactList, {
        propsData: {
            contacts: receivedContacts
        }
    })

    const item = wrapper.findAll(".prueba").wrappers

    expect(wrapper.emittedByOrder()).toEqual([])

    item[0].vm.$emit('editContact', '3')
    await wrapper.vm.$nextTick()

    expect(wrapper.emittedByOrder()).toEqual([{name:"editContactList", args:['3']}])
})

test("Comprueba que envía a App lo que recibe de ContactItem (deleteContact)", async () => {
    const receivedContacts = [
        { name: 'Nombre1', surname: "Apellido1" },
        { name: 'Nombre2', surname: "Apellido2" },
        { name: 'Nombre3', surname: "Apellido3" },
        { name: 'Nombre4', surname: "Apellido4" },
    ]
    const wrapper = shallowMount(ContactList, {
        propsData: {
            contacts: receivedContacts
        }
    })

    const item = wrapper.findAll(".prueba").wrappers

    expect(wrapper.emittedByOrder()).toEqual([])

    item[0].vm.$emit('deleteContact', '3')
    await wrapper.vm.$nextTick()

    expect(wrapper.emittedByOrder()).toEqual([{name:"deleteContact", args:['3']}])
})