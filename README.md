# front

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Run API

```
json-server --watch contact-list.json
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# contact-list

Aplicacion para gestionar una lista de contactos

##EXPLICACIÓN PROYECTO

Generar una API, utilizarla y aplicar VUE

_/INTEGRANTES/_
Yeray, Cesar y Anto

_/ESTANDAR CÓDIGO/_
Uso de formateador Prettier, con directiva de archivo .prettierrc

_/ACUERDOS/_
Por votación, o por carta mas alta

_/REPO GIT/_
https://gitlab.com/yespy13/contact-list/

_/COMANDOS GIT/_
Para poder fusionar todos los cambios que se han hecho en el repositorio local trabajando, el comando que se usa es:

PARA SUBIR A EL REPOSITORIO O PARA BAJAR DEL MISMO.

    git push  → subir al repo
    git pull    → Descargar del repo
    git add <filenames>   → Añade al commit el archivo (o archivos)
    git add . → Añade al commit todos los cambios
    git commit -m "mensaje del commit"   → commit. Esto en local
    git status → Muestra el estado en local del repositorio

Para que no nos pida la contraseña todas las veces

Ver consideraciones de seguridad en https://git-scm.com/book/es/v2/Herramientas-de-Git-Almacenamiento-de-credenciales

    git config --global credential.helper cache → Guarda la contraseña durante un tiempo
    git config --global credential.helper store → Guarda la contraseña permanentemente en ~/.git-credentials

git commit (local)
git status (verde o rojo)
git push (para subir)
e <url> → crea una copia en local del repositorio

Cotidianos

    git push  → subir al repo
    git pull    → Descargar del repo
    git add <filenames>   → Añade al commit el archivo (o archivos)
    git add . → Añade al commit todos los cambios
    git commit -m "mensaje del commit"   → commit. Esto en local
    git status → Muestra el estado en local del repositorio
